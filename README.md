# dutate-java

This library provides easy way to track your events on Dutate.
It is simple to use and fully asyncronous.

## Installation
Include the following to your project's pom.xml:
```
<dependency>
    <groupId>com.dutate.dutate</groupId>
    <artifactId>dutate-java</artifactId>
    <version>1.0</version>
</dependency>
```
If you are not using Maven you can download the library jar directly from [Maven central](https://search.maven.org/artifact/com.dutate.dutate/dutate-java/)

## Quick Start
```
import com.dutate.dutate.Dutate;

Dutate dutate = new Dutate("<your-token>");
dutate.track("event name");
```