package com.dutate.dutate;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Dutate {
    private String token;

    public Dutate(String token){
        this.token = token;
    }

    public void track(String event_name){
        track(event_name, null);
    }

    public void track(String event_name, String extra){
        final String token = this.token;
        final String jsonInputString = extra;
        final String event = event_name;
        new Thread(new Runnable() {
        	public void run() {
                try{
                    URL url = new URL(String.format("https://api.dutate.com/track/event/%s/",event));
                    HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                    httpCon.setDoOutput(true);
                    httpCon.setRequestMethod("POST");
                    httpCon.setRequestProperty("Content-Type", "application/json; utf-8");
                    httpCon.setRequestProperty("Authorization", String.format("Bearer %s",token));
                    httpCon.setRequestProperty("Accept", "application/json");
                    
                    if(jsonInputString != null){
                        OutputStream os = httpCon.getOutputStream();
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    
                    OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
                    httpCon.getResponseCode();
                    out.close();
                }catch(Exception e){
                    System.out.println("Error:"+e.toString());
                }
            }
        }).start();
    }
}
